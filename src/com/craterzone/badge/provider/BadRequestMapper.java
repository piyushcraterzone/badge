package com.craterzone.badge.provider;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.craterzone.badge.exception.BadRequestException;
import com.sun.jersey.api.client.ClientResponse.Status;

@Provider
public class BadRequestMapper implements ExceptionMapper<BadRequestException> {

	@Override
	public Response toResponse(BadRequestException exception) {
		return Response.status(Status.BAD_REQUEST)
				.entity(exception.getMessage())
				.type(MediaType.APPLICATION_JSON).build();
	}
	
	

}
