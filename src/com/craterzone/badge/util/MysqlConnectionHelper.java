package com.craterzone.badge.util;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Navrattan Yadav
 *
 */
public class MysqlConnectionHelper {
	final static Logger _logger = Logger.getLogger(MysqlConnectionHelper.class.getName());
	private String _url;
	private static MysqlConnectionHelper _instance;

	private MysqlConnectionHelper()	{
    	String driver = null;
		try {
			String fileName = "db";
            ResourceBundle resourcebundle = ResourceBundle.getBundle("projectMode");
            if(resourcebundle.getString("project.mode").equals("dev")) {
            	fileName = "localDB";
            }
            ResourceBundle bundle = ResourceBundle.getBundle(fileName);
            driver = bundle.getString("db.driver");
            Class.forName(driver);
            _url=bundle.getString("db.url");
		} catch (Exception e) {
			_logger.log (Level.SEVERE, "Unable to Load Driver"+ _url,e);
		}
	}

	public static synchronized Connection getConnection() throws SQLException {
		if (_instance == null) {
			_instance = new MysqlConnectionHelper();
		}
		try {
			return DriverManager.getConnection(_instance._url);
		} catch (SQLException e) {
			_logger.log (Level.SEVERE, "Unable to get Connection for Url"+e);
			throw e;
		}
	}
	
	public static void close(Connection connection)	{
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			_logger.log (Level.SEVERE, "Unable to Close Connection "+e);
		}
	}
	
	public static void close(Statement statement)	{
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException e) {
			_logger.log (Level.SEVERE, "Unable to Close Statement "+e);
		}
	}
	
	public static void close(ResultSet resultSet)	{
		try {
			if (resultSet != null) {
				resultSet.close();
			}
		} catch (SQLException e) {
			_logger.log (Level.SEVERE, "Unable to Close Result Set "+e);
		}
	}
	
	public static void close(PreparedStatement preparedStatement)	{
		try {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
		} catch (SQLException e) {
			_logger.log (Level.SEVERE, "Unable to Close preparedStatement "+e);
		}
	}
}