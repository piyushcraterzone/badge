package com.craterzone.badge.util;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;

import com.google.gson.Gson;

public class JsonMapper {

	public static ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		return mapper;
	}
	
	public static String convertInJson(Object object) {
		try {
			Gson gson = new Gson();
			return gson.toJson(object);
		} catch(Exception e) {
			
		}
		return "";
	}
}