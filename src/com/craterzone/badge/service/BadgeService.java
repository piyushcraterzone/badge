package com.craterzone.badge.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.craterzone.badge.enums.Associated;
import com.craterzone.badge.enums.BadgeType;
import com.craterzone.badge.model.Badge;
import com.craterzone.badge.model.User;

/**
 * 
 * Badge Service serving for list of badges
 *
 */
public class BadgeService {

	private static BadgeService _instance;

	private final Logger _logger=Logger.getLogger(BadgeService.class.getName());
	/**
	 * Constructor for Singleton
	 */
	private BadgeService() {

	}

	public static BadgeService getInstance() {

		if (_instance == null) {
			_instance = new BadgeService();
		}
		return _instance;
	}

	public List<Badge> getBadges(User user) {
		List<Badge> list=new ArrayList<Badge>();
		Badge badges=new Badge();
		badges.setAssociated(Associated.NEWFRIEND);
		badges.setBadgeType(BadgeType.NUMERIC);
		badges.setContent("3");
		list.add(badges);
		badges=new Badge();
		badges.setAssociated(Associated.PKPIN);
		badges.setBadgeType(BadgeType.TEXT);
		badges.setContent("Text Goes Here");
		list.add(badges);
		return list;
	}

}
