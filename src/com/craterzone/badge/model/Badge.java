package com.craterzone.badge.model;

import com.craterzone.badge.enums.Associated;
import com.craterzone.badge.enums.BadgeType;


/**
 * 
 * @author piyush gupta
 * 
 * A model Class for Badges
 *
 */
public class Badge {
	private String content;
	private BadgeType badgeType;
	private Associated associated;
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public BadgeType getBadgeType() {
		return badgeType;
	}
	public void setBadgeType(BadgeType badgeType) {
		this.badgeType = badgeType;
	}
	public Associated getAssociated() {
		return associated;
	}
	public void setAssociated(Associated associated) {
		this.associated = associated;
	}
	
	
	
}
