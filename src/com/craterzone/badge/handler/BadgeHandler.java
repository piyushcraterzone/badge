package com.craterzone.badge.handler;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.craterzone.badge.exception.BadRequestException;
import com.craterzone.badge.model.Badge;
import com.craterzone.badge.model.User;
import com.craterzone.badge.service.BadgeService;
import com.craterzone.badge.util.JsonMapper;
import com.sun.jersey.api.client.ClientResponse.Status;

/**
 * 
 * @author Piyush gupta
 * 
 *         A Handler Class for handling Badges Services
 * 
 */
@Path("/badge")
public class BadgeHandler {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBadge(String data) throws BadRequestException {
		List<Badge> list = null;
		try {
			User user = new User();
			ObjectMapper mapper = JsonMapper.getObjectMapper();
			User jsonUser = mapper.readValue(data, User.class);
			user.setUserId(jsonUser.getUserId());
			BadgeService badgeService = BadgeService.getInstance();
			list = badgeService.getBadges(user);
			if (list.size() > 0 && list != null) {
				return Response.status(Status.OK).entity(list).build();
			}
		} catch (JsonMappingException e) {
			throw new BadRequestException("Json Mapping Exception");
		} catch (IOException e) {
			throw new BadRequestException("Bad Request Exception");
		} catch (Exception e) {
			throw new BadRequestException("Bad Request Exception");
		}
		return Response.status(Status.NO_CONTENT).entity(list).build();
	}

}
