package com.craterzone.badge.exception;


/**
 * 
 * Custom Exception for Bad Request Exception
 *
 */
public class BadRequestException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BadRequestException(String message) {
		super(message);
	}

	public BadRequestException() {
		super("Bad Request Exception");
	}

	public BadRequestException(String message, Throwable cause) {
		super(message, cause);
	}
}
