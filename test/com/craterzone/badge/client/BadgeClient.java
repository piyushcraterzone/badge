package com.craterzone.badge.client;



import com.craterzone.badge.model.User;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class BadgeClient {

	/**
	 * @param args
	 */
	 public static void main(String[] args) {
        try {
	 
		           User user = new User();
		           user.setUserId("1");
		 	 
		 	            ClientConfig clientConfig = new DefaultClientConfig();
		 	 
		 	            clientConfig.getFeatures().put(
		 	                    JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		 	 
		 	            Client client = Client.create(clientConfig);
		 	 
		 	            WebResource webResource = client
		 	                    .resource("http://localhost:8080/BADGES_NOTIFICATION").path("/badge");
		 	            				   
		 	            
		 	            ClientResponse response = webResource.accept("application/json")
		 	                    .type("application/json").post(ClientResponse.class,user);
		 	            
		 	            if (response.getStatus() != 200) {
		 	                System.out.println("Got status:"+response.getStatus());
		 	            }
	 	            String output = response.getEntity(String.class);
	 
	            System.out.println("Server response .... \n");
	            System.out.println(output);
	 
		 	        } catch (Exception e) {
		 	 
		 	            e.printStackTrace();
		 	 
		 	        }
		 	 
		     }

}
